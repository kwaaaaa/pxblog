defmodule Pxblog.LayoutViewTest do
  use Pxblog.ConnCase, async: true

  alias Pxblog.LayoutView
  alias Pxblog.User

  @valid_attrs %{email: "[email protected]", password: "test1234", password_confirmation: "test1234", username: "testuser"}
  @current_user %{password: "test1234", username: "testuser"}

  setup do
  	User.changeset(%User{}, @valid_attrs)
  	|> Repo.insert
  	{:ok, conn: build_conn()}
  end

  test "current user returns the user in the session", %{conn: conn} do
  	conn = post conn, session_path(conn, :create), user: @current_user
  	assert LayoutView.current_user(conn)
  end

  test "current user returns nothing if there is no user in the session", %{conn: conn} do
  	user = Repo.get_by(User, %{username: @valid_attrs.username})
  	conn = delete conn, session_path(conn, :delete, user)
  	refute LayoutView.current_user(conn)
  end

  test "deleted the user session", %{conn: conn} do
  	user = Repo.get_by(User, %{username: @valid_attrs.username})
  	conn = delete conn, session_path(conn, :delete, user)
  	refute get_session(conn, :current_user)
  	assert get_flash(conn, :info) == "Signed out successfully!"
  	assert redirected_to(conn) == page_path(conn, :index)
  end

end
