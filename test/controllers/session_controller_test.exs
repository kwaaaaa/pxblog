defmodule Pxblog.SessionControllerTest do
	use Pxblog.ConnCase
	alias Pxblog.User


  @valid_create_attrs %{username: "test", password: "test",
		 password_confirmation: "test", email: "[email protected]"}
  @valid_attrs %{username: "test", password: "test"}
  @invalid_attrs %{username: "test", password: "wrong"}
  @non_existent_user_attrs %{username: "foo", password: "bar", password_digest: "kek"}


	setup do
		User.changeset(%User{}, @valid_create_attrs)
		|> Repo.insert
		{:ok, conn: build_conn()}
	end

	test "shows the login form", %{conn: conn} do
		conn = get conn, session_path(conn, :new)
		assert html_response(conn, 200) =~ "Login"
	end

	test "creates a new user session for a valid user", %{conn: conn} do
		conn = post conn, session_path(conn, :create), user: @valid_attrs
		assert get_session(conn, :current_user)
		assert get_flash(conn, :info) == "Sign in successful!"
		assert redirected_to(conn) == page_path(conn, :index)
	end

	test "does not create a session with a bad login", %{conn: conn} do
		conn = post conn, session_path(conn, :create), user: @invalid_attrs
		refute get_session(conn, :current_user)
		assert get_flash(conn, :error) == "Invalid username/password combination!"
		assert redirected_to(conn) == page_path(conn, :index)
	end

	test "does not create a session if user does not exist", %{conn: conn} do
		conn = post conn, session_path(conn, :create), user: @non_existent_user_attrs
		assert get_flash(conn, :error) == "Invalid username/password combination!"
		assert redirected_to(conn) == page_path(conn, :index)
	end
end